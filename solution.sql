


-- A.)
SELECT * FROM artists WHERE name LIKE "%d%";

-- B.)
SELECT * FROM songs where length <350;

-- C.)
SELECT album_title AS "Album Name", song_name AS "Song Name", length AS "Song Length" FROM albums 
	JOIN songs ON albums.id = songs.album_id;

-- D.)
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE album_title LIKE "%a%";

-- E.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- F.)
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY album_title DESC, song_name DESC;

-- G.)
SELECT * FROM artists
	JOIN albums ON artists.id=albums.artist_id
	WHERE name LIKE "%a%";

