
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number VARCHAR(50),
	email VARCHAR(50),
	address VARCHAR(300),
	PRIMARY KEY(id)
);


INSERT INTO users (	username, password, full_name, contact_number, email, address ) 
VALUES 
("earealAD", "ez", "Jarro Lightfeather","2147483647", "ez@league.com","Piltover"),
("seraphine", "password123", "Seraphine","2147483647", "seraphine@riot.com","loria"),
("lux", "lux123", "Luxanna Crownguard","2147483647", "lux@demacia.com","Demacia"),
("viktor", "132viktor", "Viktor","2147483647", "viktor@zaun.com","Zaun"),
("jinx", "jinx123", "Jinx","2147483647", "jinx@zaun.com","Zaun");


CREATE TABLE reviews(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	review VARCHAR(500) NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_reviews_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT	
);

INSERT INTO reviews ( review, user_id, datetime_created, rating ) 
VALUES 
("The songs are okay. Worth the subscription",1, "2023-05-03 00:00:00", 5),
("The songs are meh. I want BLACKPINK",2, "2023-01-23 00:00:00", 1),
("Add Bruno Mars and Lady Gaga",3, "2023-03-23 02:00:00", 4),
("I want to listen to more k-pop",4, "2022-09-23 04:00:00", 3),
("Kindly add more OPM",5, "2023-02-01 05:00:00", 5);


SELECT * FROM users
	JOIN reviews ON users.id = reviews.user_id
	WHERE full_name LIKE "%k%";

SELECT * FROM users
	JOIN reviews ON users.id = reviews.user_id
	WHERE full_name LIKE "%x%";

SELECT * FROM reviews
	JOIN users ON reviews.user_id = users.id

SELECT review, username FROM reviews
	JOIN users ON reviews.user_id = users.id